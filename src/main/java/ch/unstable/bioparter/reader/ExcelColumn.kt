package ch.unstable.bioparter.reader

enum class ExcelColumn(val columnNumber: Int) {
    PACKAGING(2),
    ORIGIN(3),
    QUALITY(4),
    SORTIMENT(5),
    NUMBER(6),
    UNIT(8),
    SMALL_BATCH(9),
    ORDER_BATCH(10),
    PACKAGING_BATCH(11),
    DISCOUNTED_BATCH(12),
    UNIT_PRICE(13),
    RECCOMMENDED_PRICE(14),
    MWST(16),
    EAN(17),
    INFO(18),
    HALTBARKEIT(19),
}