package ch.unstable.bioparter.reader

import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import org.apache.poi.ss.usermodel.*

import java.io.*
import java.lang.IllegalStateException
import java.util.*
import java.util.stream.IntStream
import java.util.stream.Stream

object BiopartnerReader {
    private val log: Log = LogFactory.getLog(BiopartnerReader.javaClass)
    private const val startRow = 6
    private fun getEnd(sheet: Sheet): Int {
        for (rowNum in startRow..sheet.lastRowNum) {
            val row = sheet.getRow(rowNum) ?: continue
            val cell = row.getCell(0, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL) ?: continue
            if (cell.stringCellValue == "Zertifizierung Legende") {
                return rowNum
            }
        }
        throw IllegalStateException("Couldn't find end of data")
    }

    @Suppress("MemberVisibilityCanBePrivate")
    fun readSortiment(inputStream: InputStream): Stream<BiopartnerProduct> {
        log.debug("Creating workbook")
        val wb = WorkbookFactory.create(inputStream)
        log.debug("Getting sheet")
        val sheet = wb.getSheetAt(0)
        var manufacturer: Manufacturer? = null
        var category: String? = null

        val end = getEnd(sheet)
        log.trace("row $end: Found end of data")
        return IntStream.range(startRow, end).mapToObj { rowNum ->
            val row = sheet.getRow(rowNum) ?: return@mapToObj Optional.empty<BiopartnerProduct>()
            val cell = row.getCell(0, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL) ?: return@mapToObj Optional.empty<BiopartnerProduct>()
            var value = cell.stringCellValue


            if(row.getCell(1, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL) == null) {
                if (value.startsWith("   ")) {
                    if(log.isTraceEnabled) log.trace("row $rowNum: manufacturer: $value")
                    value = value.trim { it <= ' ' }
                    if (value.isEmpty()) {
                        manufacturer = null
                    } else {
                        manufacturer = Manufacturer(value)
                    }
                } else if (value.startsWith(" ")) {
                    if(log.isTraceEnabled) log.trace("row $rowNum: category: $value")
                    category = value.trim { it <= ' ' }
                }
                Optional.empty<BiopartnerProduct>()
            } else {
                val product = BiopartnerProduct(
                        name = value.trim(),
                        packaging = row.getCell(ExcelColumn.PACKAGING)?.stringCellValue?.trim() ?: "1 Stück",
                        origin = row.getCell(ExcelColumn.ORIGIN)?.stringCellValue?.trim().let { if(it == "ZZ") null else it },
                        quality = row.getCell(ExcelColumn.QUALITY)?.stringCellValue?.trim().let { if(it == "ZZ") null else it },
                        articelCategory = row.getRequiredCell(ExcelColumn.SORTIMENT).stringCellValue.trim(),
                        category = category,
                        number = row.getRequiredCell(ExcelColumn.NUMBER).getInt(),
                        unit = row.getRequiredCell(ExcelColumn.UNIT).stringCellValue.trim(),
                        smallBatch = row.getRequiredCell(ExcelColumn.SMALL_BATCH).getFloat(),
                        orderBatch = row.getRequiredCell(ExcelColumn.ORDER_BATCH).getFloat(),
                        packagingBatch = row.getRequiredCell(ExcelColumn.PACKAGING_BATCH).getFloat(),
                        discountedBatch = row.getRequiredCell(ExcelColumn.DISCOUNTED_BATCH).getFloat(),
                        unitPrice = row.getRequiredCell(ExcelColumn.UNIT_PRICE).getFloat(),
                        manufacturer = manufacturer
                )
                if(log.isTraceEnabled) log.trace("row $rowNum: article: $product")
                return@mapToObj Optional.of(product)
            }
        }.filter { it.isPresent }.map { it.get() }

    }

    @Throws(IOException::class)
    @JvmStatic
    fun main(args: Array<String>) {
        FileInputStream("Gesamtes Sortiment.xlsx").use { inp ->
            readSortiment(inp).forEach {
                println(it)
            }
        }

    }
}

private fun Cell.getInt(): Int {
    return when (cellType) {
        CellType.STRING -> stringCellValue.toInt()
        CellType.NUMERIC -> numericCellValue.toInt()
        else -> throw IllegalStateException("Cell is not capable of storing a int: $cellType")
    }
}

private fun Cell.getFloat(): Float {
    return when (cellType) {
        CellType.STRING -> stringCellValue.replace("'", "").toFloat()
        CellType.NUMERIC -> numericCellValue.toFloat()
        else -> throw IllegalStateException("Cell is not capable of storing a float: $cellType")
    }
}

class MissingColumnsException(column: ExcelColumn, row: Row): IllegalStateException("No cell $column in row ${row.rowNum}") {

}

private fun Row.getRequiredCell(column: ExcelColumn): Cell {
    return getCell(column.columnNumber, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL) ?: throw MissingColumnsException(column, this)
}
private fun Row.getCell(column: ExcelColumn): Cell? {
    return getCell(column.columnNumber, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL)
}


