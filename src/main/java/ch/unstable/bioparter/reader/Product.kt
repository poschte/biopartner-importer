package ch.unstable.bioparter.reader

import java.text.DecimalFormat

data class BiopartnerProduct(
        val name: String,
        val packaging: String,
        val origin: String?,
        val quality: String?,
        val articelCategory: String,
        val category: String?,
        val number: Number,
        val unit: String,
        val smallBatch: Float,
        val orderBatch: Float,
        val packagingBatch: Float,
        val discountedBatch: Float,
        val unitPrice: Float,
        val manufacturer: Manufacturer?
        )


