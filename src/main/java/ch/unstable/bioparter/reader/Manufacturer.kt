package ch.unstable.bioparter.reader

data class Manufacturer(val name: String)
